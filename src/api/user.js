import express from "express";
import { check, validationResult } from "express-validator";
import User from "../models/User";
import { errors } from "../utils/errors";
import { withdrawCoins } from "../utils/config";

const router = express.Router();

router.get(
  "/get-user-details/:username",
  [
    // username must not be empty
    check("username")
      .not()
      .isEmpty(),
    // username must be a string
    check("username").isString()
  ],
  async (req, res) => {
    const validationErrors = validationResult(req);
    if (!validationErrors.isEmpty()) {
      return res
        .status(errors.STATUS_CODES.BAD_REQUEST)
        .json({ errors: validationErrors.array() });
    }
    try {
        const { username } = req.params;
        const result = await User.findOne({ username })
            .exec()
            .catch(e => {
            throw e;
            });

        if(!result) {
            throw Error("user not found")
        }
      res.json(result);
    } catch (e) {
      console.log("errors in get /get-user-details/:username : ", e);
      res
        .status(errors.STATUS_CODES.INTERNAL_SERVER_ERROR)
        .send({ msg: e.message });
    }
  }
);

router.post(
  "/get-user-name",
  [
    // address must not be empty
    check("address")
      .not()
      .isEmpty(),
    // address must be a string
    check("address").isString(),
    // coinType must not be empty
    check("coinType")
      .not()
      .isEmpty(),
    // coinType must be a string
    check("coinType").isString()
  ],
  async (req, res) => {
    const validationErrors = validationResult(req);
    if (!validationErrors.isEmpty()) {
      return res
        .status(errors.STATUS_CODES.BAD_REQUEST)
        .json({ errors: validationErrors.array() });
    }
    try {
        const { address, coinType } = req.body;
        let user = null;
        switch(coinType.toLowerCase()) {
            case 'moon':
                user = await User.findOne({ MOONAddress: address }).exec().catch(e => { throw e; });
                break;
            case 'btc':
              user = await User.findOne({ BTCAddress: address }).exec().catch(e => { throw e; });
              break;
            case 'bdt':
              user = await User.findOne({ BDTAddress: address }).exec().catch(e => { throw e; });
              break;
            case 'btc':
              user = await User.findOne({ USDTAddress: address }).exec().catch(e => { throw e; });
              break;
            default:
              throw Error("coin not available")
        }

        if(!user) {
            throw Error("user not found")
        }
        res.json(user);
    } catch (e) {
      console.log("errors in get /get-user-details/:username : ", e);
      res
        .status(errors.STATUS_CODES.INTERNAL_SERVER_ERROR)
        .send({ msg: e.message });
    }
  }
);

router.post(
    "/deposit-coins",
    [
      // username must not be empty
      check("username")
        .not()
        .isEmpty(),
      // username must be a string
      check("username").isString(),
      // coinType must not be empty
      check("coinType")
        .not()
        .isEmpty(),
      // coinType must be a string
      check("coinType").isString(),
      // amount must not be empty
      check("amount")
        .not()
        .isEmpty(),
      // amount must be a float number
      check("amount").isFloat({ min: 0 })
    ],
    async (req, res) => {
      const validationErrors = validationResult(req);
      if (!validationErrors.isEmpty()) {
        return res
          .status(errors.STATUS_CODES.BAD_REQUEST)
          .json({ errors: validationErrors.array() });
      }
      try {
            const { username, coinType, amount } = req.body;
            let user = null;
            switch(coinType.toLowerCase()) {
                case 'moon':
                    user = await User.findOneAndUpdate({ username }, { $inc: { 'MOONbalance' : amount } }, { new: true } ).exec().catch(e => { throw e; });
                    break;
                case 'btc':
                    user = await User.findOneAndUpdate({ username }, { $inc: { 'BTCbalance' : amount } }, { new: true } ).exec().catch(e => { throw e; });
                    break;
                case 'bdt':
                    user = await User.findOneAndUpdate({ username }, { $inc: { 'BDTbalance' : amount } }, { new: true } ).exec().catch(e => { throw e; });
                    break;
                case 'usdt':
                    user = await User.findOneAndUpdate({ username }, { $inc: { 'USDTbalance' : amount } }, { new: true } ).exec().catch(e => { throw e; });
                  break;
                default:
                    throw Error("coin not available")
            }

            if(!user) {
                throw Error("user not found")
            }
            res.json(user);
      } catch (e) {
        console.log("errors in get /deposit-coins : ", e);
        res
          .status(errors.STATUS_CODES.INTERNAL_SERVER_ERROR)
          .send({ msg: e.message });
      }
    }
);

router.post(
  "/withdraw",
  [
    check("username").not().isEmpty(),
    check("username").isString(),

    check("coinType").not().isEmpty(),
    check("coinType").isString(),

    check("withdrawalAddress").not().isEmpty(),
    check("withdrawalAddress").isString(),

    check("amount").not().isEmpty(),
    check("amount").isFloat({ min: 0 }),
    
  ], async (req, res) => {
    const validationErrors = validationResult(req);
    if (!validationErrors.isEmpty()) {
      return res
        .status(errors.STATUS_CODES.BAD_REQUEST)
        .json({ errors: validationErrors.array() });
    }
    try {
      const { username, coinType, amount, withdrawalAddress } = req.body;
      let user = null;
      switch(coinType.toLowerCase()) {
          case 'moon':
              await withdrawCoins(withdrawalAddress, amount, './MOON_Withdraw.sh').catch(e => { throw e; })
              user = await User.findOneAndUpdate({ username }, { $inc: { 'MOONbalance' : -1 * amount } }, { new: true } ).exec().catch(e => { throw e; });
              break;
          case 'btc':
              await withdrawCoins(withdrawalAddress, amount, './BTC_Withdraw.sh').catch(e => { throw e; })
              user = await User.findOneAndUpdate({ username }, { $inc: { 'BTCbalance' : -1 * amount } }, { new: true } ).exec().catch(e => { throw e; });
              break;
          case 'bdt':
              await withdrawCoins(withdrawalAddress, amount, './BDT_Withdraw.sh').catch(e => { throw e; })
              user = await User.findOneAndUpdate({ username }, { $inc: { 'BDTbalance' : -1 * amount } }, { new: true } ).exec().catch(e => { throw e; });
              break;
          case 'usdt':
              await withdrawCoins(withdrawalAddress, amount, './USDT_Withdraw.sh').catch(e => { throw e; })
              user = await User.findOneAndUpdate({ username }, { $inc: { 'BDTbalance' : -1 * amount } }, { new: true } ).exec().catch(e => { throw e; });
            break;    
          default:
              throw Error("coin not available")
      }

      if(!user) {
          throw Error("user not found")
      }
      res.json(user);
    } catch (e) {
      console.log("errors in post / : ", e);
      res
        .status(errors.STATUS_CODES.INTERNAL_SERVER_ERROR)
        .send({ msg: e.message });
    }
  }
);

// router.post(
//     "/withdraw-coins",
//     [
//       // username must not be empty
//       check("username")
//         .not()
//         .isEmpty(),
//       // username must be a string
//       check("username").isString(),
//       // coinType must not be empty
//       check("coinType")
//         .not()
//         .isEmpty(),
//       // coinType must be a string
//       check("coinType").isString(),
//       // amount must not be empty
//       check("amount")
//         .not()
//         .isEmpty(),
//       // amount must be a float number
//       check("amount").isFloat({ min: 0 })
//     ],
//     async (req, res) => {
//       const validationErrors = validationResult(req);
//       if (!validationErrors.isEmpty()) {
//         return res
//           .status(errors.STATUS_CODES.BAD_REQUEST)
//           .json({ errors: validationErrors.array() });
//       }
//       try {
//             const { username, coinType, amount } = req.body;
//             let user = null;
//             switch(coinType.toLowerCase()) {
//                 case 'moon':
//                     user = await User.findOneAndUpdate({ username }, { $inc: { 'MOONbalance' : -1 * amount } }, { new: true } ).exec().catch(e => { throw e; });
//                     coinName = 'MOONbalance';
//                     break;
//                 case 'btc':
//                     user = await User.findOneAndUpdate({ username }, { $inc: { 'BTCbalance' : -1 * amount } }, { new: true } ).exec().catch(e => { throw e; });
//                     break;
//                 case 'bdt':
//                     user = await User.findOneAndUpdate({ username }, { $inc: { 'BDTbalance' : -1 * amount } }, { new: true } ).exec().catch(e => { throw e; });
//                     break;
//                 default:
//                     throw Error("coin not available")
//             }

//             if(!user) {
//                 throw Error("user not found")
//             }
//             res.json(user);
//       } catch (e) {
//         console.log("errors in get /deposit-coins : ", e);
//         res
//           .status(errors.STATUS_CODES.INTERNAL_SERVER_ERROR)
//           .send({ msg: e.message });
//       }
//     }
// );

export default router;
