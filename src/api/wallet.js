import express from "express";
import { check, validationResult } from "express-validator";
import { generateBitcoinAddress, generateMoonAddress, generateBDTAddress, validateBitcoinAddress, validateEthereumAddress } from "../utils/config.js";
import { errors } from "../utils/errors";

const router = express.Router();

router.post(
  "/initiate-trade",
  [
    // inputCoinType must not be empty
    check("inputCoinType")
      .not()
      .isEmpty(),
    // outputCoinType must not be empty
    check("outputCoinType")
      .not()
      .isEmpty(),
    // outputAddress must not be empty
    check("outputAddress")
      .not()
      .isEmpty()
  ],
  async (req, res) => {
    //check for validation error
    const validationErrors = validationResult(req);
    if (!validationErrors.isEmpty()) {
      return res
        .status(errors.STATUS_CODES.BAD_REQUEST)
        .json({ errors: validationErrors.array() });
    }
    const { inputCoinType, outputAddress, outputCoinType } = req.body;

    try {
        let inputAddress = "";
        switch(inputCoinType) {
            case 'moon':
                inputAddress = await generateMoonAddress(outputAddress).catch(e => { throw e; });
                break;
            case 'btc':
                inputAddress = await generateBitcoinAddress(outputAddress).catch(e => { throw e; });
                break;
            case 'bdt':
                inputAddress = await generateBDTAddress(outputAddress).catch(e => { throw e; });
                break;
            default:
                throw Error("coin not available")

        }
        res.json({
            inputAddress,
            inputMemo: null,
            inputCoinType: inputCoinType,
            outputAddress: outputAddress,
            outputCoinType: outputCoinType,
        });
    } catch (e) {
      console.log("errors in post /initiate-trade : ", e);
      res
        .status(errors.STATUS_CODES.INTERNAL_SERVER_ERROR)
        .send({ msg: e.message });
    }
  }
);

router.post(
  "/:walletType/check-address",
  [  
    // inputCoinType must not be empty
    check("address")
      .not()
      .isEmpty()
    ],
  async (req, res) => {
    //check for validation error
    const validationErrors = validationResult(req);
    if (!validationErrors.isEmpty()) {
      return res
        .status(errors.STATUS_CODES.BAD_REQUEST)
        .json({ errors: validationErrors.array() });
    }
    const { address } = req.body;
    const { walletType } = req.params;
    let result = null;
    try {
        switch(walletType) {
            case 'bitcoin':
              result = await validateBitcoinAddress(address).catch(e => { throw e; });
                break;
            case 'ethereum':
                result = await validateEthereumAddress(address).catch(e => { throw e; });
                break;
            default:
                throw Error("wallet Type not available")

        }
        res.json(result);
    } catch (e) {
      console.log("errors in post /initiate-trade : ", e);
      res
        .status(errors.STATUS_CODES.INTERNAL_SERVER_ERROR)
        .send({ msg: e.message });
    }
  }
);

router.post(
  "/get-new-deposit-address",
  [ ],
  async (req, res) => {
    //check for validation error
    const validationErrors = validationResult(req);
    if (!validationErrors.isEmpty()) {
      return res
        .status(errors.STATUS_CODES.BAD_REQUEST)
        .json({ errors: validationErrors.array() });
    }
    const { inputCoinType, outputAddress, outputCoinType } = req.body;

    try {
        let inputAddress = "";
        switch(inputCoinType) {
            case 'moon':
                inputAddress = await generateMoonAddress(outputAddress).catch(e => { throw e; });
                break;
            case 'btc':
                inputAddress = await generateBitcoinAddress(outputAddress).catch(e => { throw e; });
                break;
            case 'bdt':
                inputAddress = await generateBDTAddress(outputAddress).catch(e => { throw e; });
                break;
            default:
                throw Error("coin not available")

        }
        res.json({
            inputAddress,
            inputMemo: null,
            inputCoinType: inputCoinType,
            outputAddress: outputAddress,
            outputCoinType: outputCoinType,
        });
    } catch (e) {
      console.log("errors in post /initiate-trade : ", e);
      res
        .status(errors.STATUS_CODES.INTERNAL_SERVER_ERROR)
        .send({ msg: e.message });
    }
  }
);

export default router;
