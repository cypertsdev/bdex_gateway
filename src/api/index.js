import express from "express";
import Coin from "../models/Coin";
import { errors } from "../utils/errors";

const router = express.Router();

router.get('/coins', async (req, res) => {
    try {
        const result = await Coin.find({}).exec().catch(e => { throw e; });
        res.json(result);
    } catch(e) {
        console.log("errors in post /initiate-trade : ", e);
      res
        .status(errors.STATUS_CODES.INTERNAL_SERVER_ERROR)
        .send({ msg: e.message });
    }
  });

export default router;
