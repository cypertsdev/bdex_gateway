import express from "express";
import { check, validationResult } from "express-validator";
import User from "../models/User";
import { errors } from "../utils/errors";
import Transaction from "../models/Transaction";

const router = express.Router();

router.post(
  "/",
  [
    check("username").not().isEmpty(),
    check("username").isString(),

    check("coinType").not().isEmpty(),
    check("coinType").isString(),

    check("withdrawalAddress").not().isEmpty(),
    check("withdrawalAddress").isString(),

    check("amount").not().isEmpty(),
    check("amount").isFloat({ min: 0 }),

    check("username").not().isEmpty(),
    check("username").isString(),
    
  ], async (req, res) => {
    const validationErrors = validationResult(req);
    if (!validationErrors.isEmpty()) {
      return res
        .status(errors.STATUS_CODES.BAD_REQUEST)
        .json({ errors: validationErrors.array() });
    }
    try {
        const result = await (new Transaction({ ...req.body })).save().catch(e => { throw e; });
        if(!result) {
            throw Error("Transaction not created")
        }
      res.json(result);
    } catch (e) {
      console.log("errors in post / : ", e);
      res
        .status(errors.STATUS_CODES.INTERNAL_SERVER_ERROR)
        .send({ msg: e.message });
    }
  }
);

router.get(
    "/pending-transactions",
    async (req, res) => {
      try {
          const result = await Transaction.find({ status: 'pending' }).exec().catch(e => { throw e; });
          if(!result) {
              throw Error("Transactions not found")
          }
        res.json(result);
      } catch (e) {
        console.log("errors in get /pending-transactions : ", e);
        res
          .status(errors.STATUS_CODES.INTERNAL_SERVER_ERROR)
          .send({ msg: e.message });
      }
    }
  );

router.put(
    "/complete-transaction/:transactionId",
    [
      // transactionId must not be empty
      check("transactionId")
        .not()
        .isEmpty(),
      // transactionId must be a string
      check("transactionId").isString(),
    ],
    async (req, res) => {
      const validationErrors = validationResult(req);
      if (!validationErrors.isEmpty()) {
        return res
          .status(errors.STATUS_CODES.BAD_REQUEST)
          .json({ errors: validationErrors.array() });
      }
      try {
            const { transactionId } = req.params;
            const result = await Transaction.findOneAndUpdate({ _id: transactionId }, { $set: { 'status' : 'completed' } }, { new: true } ).exec().catch(e => { throw e; });

            if(!result) {
                throw Error("transaction not found")
            }
            res.json(result);
      } catch (e) {
        console.log("errors in put /complete-transaction/:transactionId : ", e);
        res
          .status(errors.STATUS_CODES.INTERNAL_SERVER_ERROR)
          .send({ msg: e.message });
      }
    }
);

export default router;
