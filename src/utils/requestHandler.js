import axios from 'axios';


export function post(url, payload) {
    return axios.post(url, payload).then(({ data }) => data);
}
