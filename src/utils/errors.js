export const errors = {
    SERVER: {
        PROCESSING_ERROR: 'The server encountered an error processing the request. Please try again, Sorry for the trouble.'
    },
    STATUS_CODES: {
        BAD_REQUEST: 400,
        UNAUTHORIZED: 401,
        FORBIDDEN: 403,
        NOT_FOUND: 404,
        INTERNAL_SERVER_ERROR: 500,
        INVALID_TOKEN: 498,
        CONFLICT: 409,
        ENTITY_ALREADY_EXIST: 422,
        SUCCESS: 200,
        ERROR: 301
    },
    RESPONSE_MESSAGES: {
        INTERNAL_SERVER_ERROR: 'Internal Server Error',
        INVALID_TOKEN: 'Invalid token',
        FORBIDDEN: 'Forbidden: Access is denied',
        ERROR: 'The server encountered an error processing the request. Please try again, Sorry for the trouble.',
        APP_SETTINGS: {
            NOT_FOUND: 'App setting not found'
        },
        USER: {
            UNAUTHORIZED: 'User is unauthorized',
            DUPLICATE: 'User already present',
            INCORRECT_PASSWORD: 'Old password is incorrect',
            NOT_FOUND: 'User is not present',
            INVALID_PASSWORD: 'Wrong password',
            RESET_PASSWORD_EXPIRED: 'Password reset token is invalid or has expired',
            SAME_PASSWORD: 'Old password and new password should not be same',
            SCOPES: 'Can not fetch scopes of user',
            GROUP_SCOPES: 'Can not fetch group scopes',
            INVOICE: 'Can not fetch invoices',
            SUCCESS: 'User added successfully.'
        }
    }
};
