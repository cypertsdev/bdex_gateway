import SSH from "ssh2-promise";
import User from "../models/User";

const host = "185.233.186.20";
const username = "root";
const password = "juFvxhW2BcCPd5tWfsPX4Q1h";

const ssh = new SSH({ username, host, password });

export const generateBitcoinAddress = async (username) => {

    try { 
        let user = await User.findOne({ username }).exec().catch(e => { throw e; });
        if(user && user.BTCAddress) {
            return user.BTCAddress;
        } 

        // user is not present so create one
        if(!user) {
            user = await (new User({ username })).save().catch(e => { throw e; });
        }

        await ssh.connect();
        const address = await ssh.exec(`bitcoin-cli getnewaddress ${username}`).catch(e => { throw e; });
        const BTCAddress = address.replace(/(\r\n|\n|\r)/gm, "");
        await User.findOneAndUpdate({ username }, { BTCAddress });
        return BTCAddress;
    } catch(e) {
        throw e;
    }
}; 

export const generateMoonAddress = async (username) => {

    try { 
        let user = await User.findOne({ username }).exec().catch(e => { throw e; });
        if(user && user.MOONAddress) {
            return user.MOONAddress;
        } 

        // user is not present so create one
        if(!user) {
            user = await (new User({ username })).save().catch(e => { throw e; });
        }

        await ssh.connect();
        const address = await ssh.exec(`./realone.sh ${username} | grep "Public address of the key:"`).catch(e => { throw e; });;
        const MOONAddress = address.replace(/(\r\n|\n|\r)/gm, "").split(" ").pop();
        await User.findOneAndUpdate({ username }, { MOONAddress });
        return MOONAddress;
    } catch(e) {
        throw e;
    }
}; 

export const generateBDTAddress = async (username) => {

    try { 
        let user = await User.findOne({ username }).exec().catch(e => { throw e; });
        if(user && user.BDTAddress) {
            return user.BDTAddress;
        } 

        // user is not present so create one
        if(!user) {
            user = await (new User({ username })).save().catch(e => { throw e; });
        }

        await ssh.connect();
        const address = await ssh.exec(`./realone.sh ${username} | grep "Public address of the key:"`).catch(e => { throw e; });;
        const BDTAddress = address.replace(/(\r\n|\n|\r)/gm, "").split(" ").pop();
        await User.findOneAndUpdate({ username }, { BDTAddress });
        return BDTAddress;
    } catch(e) {
        throw e;
    }
}; 

export const validateBitcoinAddress = async (btcAddress) => {
    try {
        await ssh.connect().catch(e => { throw e; });
        let result = await ssh.exec(`bitcoin-cli validateaddress ${btcAddress}`).catch(e => { throw e; });
        result = JSON.parse(result)
        return { isValid: result.isvalid };
    } catch(e) {
        throw e;
    }
}; 

export const validateEthereumAddress = async (bdtAddress) => {
    return { isValid: bdtAddress.startsWith("0x") && bdtAddress.length == 42 }; 
}

export const withdrawCoins = async (address, amount, scriptName ) => {
    try { 
        await ssh.connect();
        const resp = await ssh.exec(`${scriptName} ${address} ${amount}`).catch(e => { throw e; });;
        return resp;
    } catch(e) {
        throw Error(e.toString('utf8'));
    }
}; 
