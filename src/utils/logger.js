var winston = require('winston');
var moment = require('moment');

function formatter(args) {
    var date = moment().format('DD/MM/YYYY hh:mm:ss');
    var msg = date + ' - ' + args.level + ' - ' + args.message + ' - ' + JSON.stringify(args.meta, null, 2);
    return msg;
}

var logger = new winston.createLogger({
    transports: [
        new winston.transports.Console({
            handleExceptions: true,
            humanReadableUnhandledException: true,
            json: false,
            colorize: true,
            level: 'debug',
            formatter: formatter
        })
    ],
    exitOnError: false
});

module.exports = logger;
module.exports.stream = {
    write: function (message, encoding) {
        console.log(message);
    }
};
