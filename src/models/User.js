var mongoose = require("mongoose");

const userSchema = mongoose.Schema(
  {
    username: {
      type: String,
      unique: true,
      trim: true,
      lowercase: true,
      required: true
    },
    BTCAddress: {
      type: String
    },
    BTCbalance: {
      type: Number,
      default: 0,
    },
    BDTAddress: {
      type: String
    },
    BDTbalance: {
      type: Number,
      default: 0,
    },
    MOONAddress: {
      type: String
    },
    MOONbalance: {
      type: Number,
      default: 0,
    },
    USDTAddress: {
      type: String
    },
    USDTbalance: {
      type: Number,
      default: 0,
    }
  },
  {
    timestamps: true
  }
);

const User = mongoose.model("User", userSchema);

export default User;
