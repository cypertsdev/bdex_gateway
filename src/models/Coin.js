var mongoose = require("mongoose");

const coinSchema = mongoose.Schema(
  {
    name: {
      type: String
    },
    description: {
      type: String
    },
    backingCoin: {
      type: String
    },
    symbol: {
      type: String
    },
    depositAllowed: {
      type: Boolean,
      default: false
    },
    withdrawalAllowed: {
      type: Boolean,
      default: false
    },
    memoSupport: {
      type: Boolean,
      default: false
    },
    precision: {
      type: Number
    },
    issuer: {
      type: String
    },
    issuerId: {
      type: String
    },
    walletType: {
      type: String
    },
    minAmount: {
      type: Number
    },
    withdrawFee: {
      type: Number
    },
    depositFee: {
      type: Number
    },
    confirmations: {
      type: {
        type: String
      },
      value: {
        type: Number
      }
    }
  },
  {
    timestamps: true
  }
);

const Coin = mongoose.model("coin", coinSchema);

export default Coin;
