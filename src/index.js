import bodyParser from 'body-parser';
import compression from 'compression';
import timeout from 'connect-timeout';
import cookieParser from 'cookie-parser';
import dotenv from 'dotenv';
import express from 'express';
import { Server } from 'http';
import mongoose from 'mongoose';
import morgan from 'morgan';
import index from './api/index';
// import transaction from './api/transaction';
import user from './api/user';
import wallet from './api/wallet';
// import { errors } from './utils/errors';
import logger from './utils/logger';
import path from 'path';

dotenv.load();

let app = express();
let server = Server(app);

app.use(compression());
app.use(timeout('80s'));
app.use(express.static('dist'));
app.use(express.static('public'));

app.use(cookieParser());
app.use(bodyParser.json());

app.use(morgan('dev', {
    stream: logger.stream
}));

app.use(bodyParser.urlencoded({
    extended: true
}));

// configure our app to handle CORS requests
app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers',
        'Content-Type, Content-Disposition, Content-Description, Content-Range, Authorization, X-Requested-With, Cache-Control, Accept, Origin, X-Session-ID, socketID');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('X-Frame-Options', 'sameorigin');
    next();
});

// routes
app.use('/', index);
app.use('/simple-api', wallet);
app.use('/user', user);
// app.use('/transaction', transaction);

mongoose.connect(process.env.MONGO_URI,
    {
        useNewUrlParser: true

    })
    .then(() => console.log('Succeeded connected to: ' + process.env.MONGO_URI))
    .catch(err => console.log('ERROR connecting to: ' + process.env.MONGO_URI + '. ' + err));

// If the Node process ends, close the Mongoose connection
process.on('SIGINT', function () {
    mongoose.connection.close().then(() => process.exit(0));
});

app.use('/*', (req, res) => res.sendFile(__dirname.replace('/src', '') + '/dist/index.html'));

app.use((err, req, res, next) => {
    if(err.status == 404) {
        console.log(path.resolve('/dist/index.html'), __dirname.replace('/src', '') + '/dist/index.html')
        res.sendFile(__dirname.replace('/src', '') + '/dist/index.html');
    }
    res.status(err && err.status || 500);
    if (app.get('env') !== 'development') {
        delete err.stack;
    }
    res.send({ message: err.message });
});


server.listen(process.env.PORT || 29293, () => {
    console.log('Express server listening on port ' + (process.env.PORT || 29293));
});

export default app;
